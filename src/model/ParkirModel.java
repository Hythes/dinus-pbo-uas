package model;

import java.sql.Date;
import java.sql.Time;

public class ParkirModel {
	private int id;
	private String nopol;
	private String tipe;
	private Date tanggal;
	private Time jam_masuk;
	private Time jam_keluar;
	private int biaya;

	
	public ParkirModel(int id, String nopol, String tipe, Date tanggal, Time jam_masuk, Time jam_keluar, int biaya) {
		this.id = id;
		this.nopol = nopol;
		this.tipe = tipe;
		this.tanggal = tanggal;
		this.jam_keluar = jam_keluar;
		this.jam_masuk = jam_masuk;
		this.biaya = biaya;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNopol() {
		return nopol;
	}

	public void setNopol(String nopol) {
		this.nopol = nopol;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public Time getJamMasuk() {
		return jam_masuk;
	}

	public void setJamMasuk(Time jamMasuk) {
		this.jam_masuk = jamMasuk;
	}

	public Time getJamKeluar() {
		return jam_keluar;
	}

	public void setJamKeluar(Time jamKeluar) {
		this.jam_keluar = jamKeluar;
	}

	public Integer getBiaya() {
		return biaya;
	}

	public void setBiaya(Integer biaya) {
		this.biaya = biaya;
	}

}
