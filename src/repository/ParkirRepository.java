package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.ParkirModel;
import utils.DB;

public class ParkirRepository {
	public String[] cb_jenis_model = { "Motor", "Mobil", "Truk" };
	public int[] cb_jenis_biaya = { 2000, 4000, 5000 };

	private DB conn;
	private Statement stmt;
	private PreparedStatement pstmt;
	private ResultSet rs;

	public ParkirRepository() {
		conn = new DB();
	}

	public boolean create(String no, int index_tipe, Date tanggal) {
		try {

			int biaya = cb_jenis_biaya[index_tipe];
			String tipe = cb_jenis_model[index_tipe];
			Time jam = new Time(tanggal.getTime());

			String sql = "INSERT INTO parkir(nopol,tipe,tanggal,jam_masuk) VALUES(?,?,?,?);";
			pstmt = conn.prepareStatement(sql);
			java.sql.Date tgl = new java.sql.Date(tanggal.getTime());

			pstmt.setString(1, no);
			pstmt.setString(2, tipe);
			pstmt.setDate(3, tgl);
			pstmt.setTime(4, jam);

			if (!pstmt.execute())
				return false;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public DefaultTableModel getAll() {
		try {
			DefaultTableModel dtm = new DefaultTableModel();
			stmt = conn.createStatement();
			dtm.addColumn("ID");
			dtm.addColumn("No. Pol");
			dtm.addColumn("Jenis Kendaraan");
			dtm.addColumn("Tanggal");
			dtm.addColumn("Jam Masuk");
			dtm.addColumn("Jam Keluar");
			dtm.addColumn("Biaya");

			rs = stmt.executeQuery("SELECT * FROM parkir");
			while (rs.next()) {

				dtm.addRow(new Object[] { rs.getString("id"), rs.getString("nopol"), rs.getString("tipe"),
						rs.getString("tanggal"), rs.getString("jam_masuk"), rs.getString("jam_keluar"),
						rs.getString("biaya_parkir") });
			}

			return dtm;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String[] getOne(String id) {
		try {

			pstmt = conn.prepareStatement("SELECT * FROM parkir WHERE id = ?");
			pstmt.setString(1, id);
			pstmt.execute();

			rs = pstmt.getResultSet();
			rs.next();

			String[] obt = new String[] { rs.getString("id"), rs.getString("nopol"), rs.getString("tipe"),
					rs.getString("tanggal"), rs.getString("jam_masuk"), rs.getString("jam_keluar"),
					rs.getString("biaya_parkir") };
			return obt;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	public void delete(String id) {
		try {

			pstmt = conn.prepareStatement("DELETE FROM parkir WHERE id = ?");
			pstmt.setString(1, id);
			pstmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();

		}

	}

	public boolean update(String id, String nopol, String jenis) {
		try {

			pstmt = conn.prepareStatement("UPDATE  parkir SET nopol = ? ,tipe = ?  WHERE id = ?");
			pstmt.setString(1, nopol);
			pstmt.setString(2, jenis);
			pstmt.setString(3, id);
			pstmt.execute();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}

	}

	public void updateBiaya(String id, String jamkel, String biaya, String nopol, String jenis) {
		try {

			pstmt = conn.prepareStatement(
					"UPDATE  parkir SET jam_keluar = ? ,biaya_parkir = ?, nopol =? ,tipe= ?  WHERE id = ?");
			pstmt.setString(1, jamkel);
			pstmt.setString(2, biaya);
			pstmt.setString(3, nopol);
			pstmt.setString(4, jenis);
			pstmt.setString(5, id);
			pstmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();

		}

	}
}
