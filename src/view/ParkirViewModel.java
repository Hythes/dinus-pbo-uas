package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.table.DefaultTableModel;

import model.ParkirModel;
import repository.ParkirRepository;

public class ParkirViewModel {
	public String[] cb_jenis_model = { "Motor", "Mobil", "Truk" };
	public int[] cb_jenis_biaya = { 2000, 4000, 5000 };

	private ParkirRepository model;

	private String changeMonth(int month) {
		String[] mt = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September",
				"Oktober", "November", "Desember" };
		String r = mt[month - 1];
		return r;

	}
	public void delete(String id) {
		model.delete(id);
		
	}
	public ParkirViewModel() {
		model = new ParkirRepository();
	}

	public boolean jamMasuk(String no, int index_tipe, Date tanggal) {
		boolean insertData = model.create(no, index_tipe, tanggal);
		if (insertData)
			return true;
		return false;
	}

	public DefaultTableModel getAll() {
		DefaultTableModel dtm = model.getAll();
		return dtm;
	}

	public String getTanggalFormatted() {
		Calendar cl = Calendar.getInstance();

		int tanggal = cl.get(Calendar.DAY_OF_MONTH);
		int bulan = cl.get(Calendar.MONTH);
		int tahun = cl.get(Calendar.YEAR);

		String s = "" + tanggal + " " + changeMonth(bulan) + " " + tahun;

		return s;

	}

	public void jamKeluar(String id, String nopol, int idx, String tanggal, String jam_masuk) {
		String jamKeluar = this.getJam();
		String jenis = cb_jenis_model[idx];
		String totalBiaya = this.getBiaya(jam_masuk, idx);
		System.out.println("Jam Masuk : " + jam_masuk + " | Jam Keluar : " + jamKeluar +  "| Total Biaya :" + totalBiaya);
		

		model.updateBiaya(id, jamKeluar, totalBiaya + "", nopol, jenis);

	}
	public String getBiaya(String jam_masuk,int idx) {
		String jenis = cb_jenis_model[idx];
		int biaya = cb_jenis_biaya[idx];
		String jamKeluar = this.getJam();
		double totalJam = this.compareJam(jam_masuk, jamKeluar);
		double totalBiaya = Math.ceil(totalJam * biaya);
		String b = totalBiaya+"";
		return b;
	}

	public String[] getOneData(String nopol) {
		String[] br = model.getOne(nopol);
		return br;
	}

	public Date getTanggal() {
		Calendar cl = Calendar.getInstance();
		return cl.getTime();
	}

	public String getJamFormatted() {
		Calendar cl = Calendar.getInstance();

		int jam = cl.get(Calendar.HOUR_OF_DAY);
		int menit = cl.get(Calendar.MINUTE);

		String ret = "" + jam + ":" + menit;

		return ret;
	}

	public String getJam() {
		Calendar cl = Calendar.getInstance();

		int jam = cl.get(Calendar.HOUR_OF_DAY);
		int menit = cl.get(Calendar.MINUTE);
		int detik = cl.get(Calendar.SECOND);

		String ret = "" + jam + ":" + menit + ":" + detik;

		return ret;
	}

	public long compareJam(String jam_masuk, String jam_keluar) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
			Date date1 = format.parse(jam_masuk);
			Date date2 = format.parse(jam_keluar);
			long difference = date2.getTime() - date1.getTime();
			long menit
            = (difference
               / (1000 * 60))
              % 60;
        long jam
            = (difference
               / (1000 * 60 * 60))
              % 24;
        
        	if(menit > 0) {
        		jam = jam+1;
        	}
        
			return jam;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	public boolean editData(String id, String nopol, int index) {
		String jens = cb_jenis_model[index];
		boolean update_data = model.update(id, nopol, jens);
		if (update_data)
			return true;
		return false;
	}

	public int getIndexJenisKendaraan(String jenis) {
		for (int i = 0; i < cb_jenis_model.length; i++) {

			if (cb_jenis_model[i].equals(jenis)) {
				return i;
			}
		}
		return 0;
	}

}
