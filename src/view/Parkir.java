package view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.font.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;

public class Parkir {

	public JFrame frame;
	private JTextField tf_nopol;
	private JTable tb_isi;
	private ParkirViewModel vm;
	private JLabel lbl_title;
	private JLabel lbl_tanggal;
	private JLabel lbl_jam;
	private JLabel lbl_nopol;
	private JLabel lbl_jenis;
	private JComboBox cb_jenis;
	private JLabel lbl_biaya;
	private JLabel lbl_out_biaya;
	private JButton btn_jam_masuk;
	private JButton btn_jam_keluar;
	private JButton btn_edit;
	private JButton btn_delete;
	private JButton btn_cetak;
	private JPanel p_tanggal;
	private JPanel p_input;
	private JPanel p_output;
	private JPanel p_button;
	private JScrollPane sp_tabel;
	private Date v_tanggal;
	private DefaultTableModel dtm_isi;

	private String v_id;
	private String v_jam_masuk;
	private String v_jam_keluar;
	private String v_tanggal_db;
	private String v_biaya;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Parkir window = new Parkir();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Parkir() {
		vm = new ParkirViewModel();
		initialize();
		initCode();
		initButton();
	}

	public void initButton() {
		btn_jam_masuk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vm.jamMasuk(tf_nopol.getText(), cb_jenis.getSelectedIndex(), v_tanggal);
				initCode();
			}
		});

		btn_jam_keluar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vm.jamKeluar(v_id, tf_nopol.getText(), cb_jenis.getSelectedIndex(), v_tanggal_db, v_jam_masuk);
				initCode();
			}
		});
		btn_edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean bl = vm.editData(v_id, tf_nopol.getText(), cb_jenis.getSelectedIndex());
				initCode();
			}
		});
		
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vm.delete(v_id);
				initCode();
			}
		});
		
		btn_cetak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ParkirReport rt = new ParkirReport();
				rt.frame.setVisible(true);
				
			}
		});

		tb_isi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String id = tb_isi.getValueAt(tb_isi.getSelectedRow(), 0).toString();

				getDataToView(id);

			}
		});

	}

	public void getDataToView(String id) {
		String[] data = vm.getOneData(id);
		v_id = data[0];
		tf_nopol.setText(data[1]);
		cb_jenis.setSelectedIndex(vm.getIndexJenisKendaraan(data[2]));
		v_tanggal_db = data[3];
		v_jam_masuk = data[4];
		v_jam_keluar = data[5];
		v_biaya = data[6];
		lbl_out_biaya.setText("Rp. "+ v_biaya);
		if (v_biaya == null) {
			lbl_out_biaya.setText("Rp. "+vm.getBiaya(v_jam_masuk, cb_jenis.getSelectedIndex()));
		}
	

	}

	public void initCode() {
		v_tanggal = vm.getTanggal();
		lbl_tanggal.setText("Tanggal : " + vm.getTanggalFormatted());
		lbl_jam.setText("Jam : " + vm.getJamFormatted());
		tf_nopol.setText("");
		cb_jenis.setSelectedIndex(0);
		lbl_out_biaya.setText("Rp. 0");
		// Get Data
		dtm_isi = vm.getAll();
		tb_isi.setModel(dtm_isi);

		// Refresh Data
		v_id = "";
		v_tanggal_db = "";
		v_jam_masuk = "";
		v_jam_keluar = "";
		v_biaya = "";

	}

	public DefaultTableModel createHeaderTable(DefaultTableModel dtm2) {

		return dtm2;

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.BLACK);
		frame.getContentPane().setBackground(Color.ORANGE);
		frame.setResizable(false);
		frame.setBounds(100, 100, 872, 659);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		lbl_title = new JLabel("Sistem Parkir PT XYZ");
		lbl_title.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lbl_title.setBounds(276, 26, 312, 29);
		frame.getContentPane().add(lbl_title);

		p_tanggal = new JPanel();
		p_tanggal.setBackground(Color.ORANGE);
		p_tanggal.setBounds(77, 66, 708, 73);
		frame.getContentPane().add(p_tanggal);

		lbl_tanggal = new JLabel("Tanggal");
		lbl_tanggal.setFont(new Font("Times New Roman", Font.BOLD, 20));

		lbl_jam = new JLabel("Jam");
		lbl_jam.setFont(new Font("Times New Roman", Font.BOLD, 20));

		JLabel label = new JLabel("");
		GroupLayout gl_p_tanggal = new GroupLayout(p_tanggal);
		gl_p_tanggal.setHorizontalGroup(gl_p_tanggal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_p_tanggal.createSequentialGroup().addGap(19)
						.addComponent(lbl_tanggal, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGap(319).addComponent(lbl_jam, GroupLayout.PREFERRED_SIZE, 128,

								GroupLayout.PREFERRED_SIZE)
						.addGap(59)));
		gl_p_tanggal.setVerticalGroup(gl_p_tanggal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_p_tanggal.createSequentialGroup().addGap(29).addGroup(gl_p_tanggal
						.createParallelGroup(Alignment.BASELINE).addComponent(lbl_tanggal).addComponent(lbl_jam))));
		p_tanggal.setLayout(gl_p_tanggal);

		p_input = new JPanel();
		p_input.setBackground(Color.ORANGE);
		p_input.setBounds(77, 164, 285, 88);
		frame.getContentPane().add(p_input);
		p_input.setLayout(null);

		lbl_nopol = new JLabel("No. Polisi");
		lbl_nopol.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lbl_nopol.setBounds(10, 19, 86, 19);
		p_input.add(lbl_nopol);

		tf_nopol = new JTextField();
		tf_nopol.setFont(new Font("Times New Roman", Font.PLAIN, 11));
		tf_nopol.setBounds(137, 11, 105, 27);
		p_input.add(tf_nopol);
		tf_nopol.setColumns(10);

		lbl_jenis = new JLabel("Jenis Kendaraan");
		lbl_jenis.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lbl_jenis.setBounds(0, 41, 113, 36);
		p_input.add(lbl_jenis);

		cb_jenis = new JComboBox();
		cb_jenis.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		cb_jenis.setModel(new DefaultComboBoxModel(vm.cb_jenis_model));
		cb_jenis.setBounds(137, 49, 105, 28);
		p_input.add(cb_jenis);

		p_output = new JPanel();
		p_output.setBackground(Color.ORANGE);
		p_output.setBounds(563, 164, 222, 84);
		frame.getContentPane().add(p_output);
		p_output.setLayout(null);

		lbl_biaya = new JLabel("Biaya");
		lbl_biaya.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lbl_biaya.setBounds(83, 11, 53, 32);
		p_output.add(lbl_biaya);

		lbl_out_biaya = new JLabel("Rp. 0");
		lbl_out_biaya.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lbl_out_biaya.setBounds(10, 43, 202, 27);
		p_output.add(lbl_out_biaya);

		p_button = new JPanel();
		p_button.setBackground(Color.ORANGE);
		p_button.setBounds(77, 287, 708, 88);
		frame.getContentPane().add(p_button);
		p_button.setLayout(null);

		btn_jam_masuk = new JButton("Jam Masuk");
		btn_jam_masuk.setFont(new Font("Times New Roman", Font.BOLD, 11));

		btn_jam_masuk.setBounds(10, 21, 104, 45);
		p_button.add(btn_jam_masuk);

		btn_jam_keluar = new JButton("Jam Keluar");
		btn_jam_keluar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_jam_keluar.setFont(new Font("Times New Roman", Font.BOLD, 11));
		btn_jam_keluar.setBounds(145, 21, 104, 45);
		p_button.add(btn_jam_keluar);

		btn_edit = new JButton("Edit");
		btn_edit.setFont(new Font("Times New Roman", Font.BOLD, 11));
		btn_edit.setBounds(286, 21, 104, 45);
		p_button.add(btn_edit);

		btn_delete = new JButton("Delete");
		btn_delete.setFont(new Font("Times New Roman", Font.BOLD, 11));
		btn_delete.setBounds(441, 21, 104, 45);
		p_button.add(btn_delete);

		btn_cetak = new JButton("Cetak Tiket");
		btn_cetak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_cetak.setFont(new Font("Times New Roman", Font.BOLD, 11));
		btn_cetak.setBounds(594, 21, 104, 45);
		p_button.add(btn_cetak);

		sp_tabel = new JScrollPane();
		sp_tabel.setBounds(77, 443, 708, 131);
		frame.getContentPane().add(sp_tabel);

		dtm_isi = new DefaultTableModel();

		tb_isi = new JTable(dtm_isi);
		tb_isi.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		tb_isi.setDefaultEditor(Object.class, null);
		sp_tabel.setViewportView(tb_isi);
	}
}
