package utils;

import java.sql.*;

public class DB {
	// SQL
	public static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://127.0.0.1/uas_pbo";
	public static final String USER = "root";
	public static final String PASS = "";

	public static Connection conn;

	public DB() {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Statement createStatement() {
		try {
			Statement stmt2;

			stmt2 = conn.createStatement();
			return stmt2;
		} catch (SQLException e) {

			e.printStackTrace();
			return null;
		}
	}

	public void closeStatement(Statement sm) {
		try {
			sm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public PreparedStatement prepareStatement(String sql) {
		try {
			return conn.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
