# SISTEM PARKIR TUGAS AKHIR PBO

## Dibuat oleh : 

1. Hanif Nouval Setyananda (A11.2020.12893)
2. Anggoro Ajie Putranto (A11.2020.13202)
3. Rizky Raflian (A11.2020.12916)

## Cara Install

- Gunakan IDE Eclipse
- Gunakan JavaSE-17
- Buka `utils/DB.java` dan rubah Database, Username, Password
- Gunakan [mysql-connector-java-8.0.17.jar](https://jar-download.com/artifacts/mysql/mysql-connector-java/8.0.17/source-code) Untuk connector mysql

## Struktur Aplikasi

Rencana menggunakan model MVVM tetapi tidak jadi, padahal sudah terlanjur dibuat, akhirnya seadanya saja...

### Model

Tidak dipakai, rencana untuk menyimpan data yang diambil dari database

### Repository

Untuk melakukan segala koneksi ke Database

### Utils

Untuk konfigurasi ke database

### views

terdapat view dan ViewModel, View hanya sebagai tampilan, logic berada di ViewModel


